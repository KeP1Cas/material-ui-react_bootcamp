import { createMuiTheme } from "@material-ui/core"

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#0F4780",
    },
    secondary: {
      main: "#6B64FF",
    },
  },

  overrides: {
    MuiCssBaseline: {
      "@global": {
        body: {
          background: "#12c2e9",
          background: "linear-gradient(to right, #f64f59, #c471ed, #12c2e9)",
        },
      },
    },
    MuiButton: {
      root: {
        width: "100%",
        padding: "10px 16px",
      },
      containedSecondary: {
        "&:hover": {
          opacity: "0%",
        },
      },
    },

    typography: {
      button: {
        fontSize: "16px",
        fontWeight: "bold",
      },
    },

    MuiPaper: {
      root: {
        width: "400px",
        padding: "20px",
        backgroundColor: "#e1bee7",
      },
      rounded: {
        borderRadius: "8px",
      },
    },
  },
  spacing: 4,

  breakpoints: {
    values: {
      sm: 700,
    },
  },
})

export { theme }
