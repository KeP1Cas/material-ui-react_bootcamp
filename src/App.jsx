import React from "react"
import { ThemeProvider } from "@material-ui/core/styles"
import { Grid, Button, Paper, CssBaseline } from "@material-ui/core"

import { theme } from "./Theme"
import MuiCustomButton from "./components/Button/MuiCustomButton"

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Grid
        container
        justify="center"
        alignItems="center"
        spacing={0}
        style={{ minHeight: "100vh" }}
      >
        <Paper color="primary">
          <Grid container spacing={4}>
            <Grid item xs={12} sm={6}>
              <Button variant="contained" color="primary">
                Primary
              </Button>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Button variant="contained" color="secondary">
                Transparent
              </Button>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Button variant="contained">Default</Button>
            </Grid>

            <Grid item xs={12} sm={6}>
              <MuiCustomButton error />
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </ThemeProvider>
  )
}

export default App
