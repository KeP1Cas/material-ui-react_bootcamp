import React from "react"
import { Button } from "@material-ui/core"
import { makeStyles } from "@material-ui/core"

const stylesError = makeStyles((theme) => ({
  error: {
    backgroundColor: theme.palette.error.main,
    "&:hover": {
      backgroundColor: "#b71c1c",
    },
  },
}))

const MuiCustomButton = ({ error }) => {
  const classes = stylesError()
  return (
    <Button className={error && classes.error} variant="contained">
      Error
    </Button>
  )
}

export default MuiCustomButton
